import { http } from './index'
import {Message} from 'element-ui'
//获取菜单权限列表
export function getMenus(){
    return http('menus','get').then(res=>{
        return res.data
    })
}
// 获取用户列表
export function getUsers(params){
    return http("users","get",{},params).then(res=>{
        return res.data
    })
}
//添加用户 
export function addUsers(data){
    return http('users','post',data).then(res=>{
        Message({
            type:'success',
            message:'添加用户成功'
        })
        return res.data
    })
}

//修改用户状态
export function updateUserState(data){
    return http(`users/${data.uid}/state/${data.type}`,'put').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//编辑用户信息
export function editUsers(data){
    return http(`users/${data.id}`,'put',{email:data.email,mobile:data.mobile}).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//删除用户
export function deleteUsers(id){
    return http(`users/${id}`,'delete',).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//查询用户信息
export function getUsersInfo(id){
    return http(`users/${id}`).then(res=>{
        return res.data
    })
}
//获取角色列表
export function getRolesList(){
    return http(`roles`).then(res=>{
        return res.data
    })
}
//设置角色
export function distriRoles(id,rid){
    return http(`users/${id}/role`,'put',{
        id,
        rid
    }).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}

//添加角色
export function addRoles(data){
    return http(`roles`,'post',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//删除权限
export function deleteRights(roleId,rightId){
    return http(`roles/${roleId}/rights/${rightId}`,'delete').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//编辑角色
export function editRights(id,data){
    return http(`roles/${id}`,'put',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//获取权限列表
export function getRigets(type='tree'){
    return http(`rights/${type}`,'get').then(res=>{
        return res.data
    })
}
//角色授权
export function setRigets(id,rids){
    return http(`roles/${id}/rights`,'post',{rids}).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//删除角色
export function deleteRigets(id){
    return http(`roles/${id}`,'delete').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//获取商品列表
export function getGoods(data){
    return http(`goods`,'get',{},data).then(res=>{
        return res.data
    })
}
//获取商品分类列表
export function getCategoriesList(arr){
    return http(`categories`,'get',{},arr).then(res=>{
        return res.data
    })
}
//获取参数列表
export function getAttributesList(id,sel='many'){
    return http(`categories/${id}/attributes`,'get',{},{sel}).then(res=>{
        return res.data
    })
}
//添加商品
export function addGoods(data){
    return http(`goods`,'post',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//查询商品
export function queryGoods(id){
    return http(`goods/${id}`,'get').then(res=>{
        return res.data
    })
}
//编辑商品
export function editGoods(id,data){
    return http(`goods/${id}`,'put',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//删除商品
export function deleteGoods(id){
    return http(`goods/${id}`,'delete').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//编辑提交参数
export function editAttributes(id,attrid,data){
    return http(`categories/${id}/attributes/${attrid}`,'put',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//删除参数
export function deleteAttributes(id,attrid){
    return http(`categories/${id}/attributes/${attrid}`,'delete').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//添加参数
export function addAttributes(id,data){
    return http(`categories/${id}/attributes`,'post',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//添加分类
export function addCategories(data){
    return http(`categories`,'post',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//编辑参数
export function queryCategories(data){
    return http(`categories/${data.cat_id}`,'put',{cat_name:data.cat_name}).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//删除参数
export function deleteCategories(id){
    return http(`categories/${id}`,'delete').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//获取订单列表
export function getOrders(data){
    return http(`orders`,'get',{},data).then(res=>{
        return res.data
    })
}
//修改订单信息
export function putOrders(data){
    return http(`orders/${data.id}`,'put',data).then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}
//获取数据报表
export function getReports(type){
    return http(`reports/type/${type}`,'get').then(res=>{
        Message({
            type:'success',
            message:res.meta.msg
        })
        return res.data
    })
}