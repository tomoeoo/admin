import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    username:localStorage.getItem("username")||"",
    token:localStorage.getItem("token")||"",
  },
  mutations: {
    setUsername(state,sty){
      state.username=sty;
      localStorage.setItem("username",sty)
    },
    setToken(state,sty){
      state.token=sty;
      localStorage.setItem("token",sty)
    },
    initState(state,data){
      state.username=""
      state.token=""
      localStorage.clear()
    }
  },
  actions: {
  },
  modules: {
  }
})
