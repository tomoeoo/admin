import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import Login from '@/views/Login'
import store from "@/store"
Vue.use(VueRouter)

  const routes = [
    {
      path:'/',
      redirect:'/login'
    },
    {
      path:'/login',
      name:'login',
      component:Login,
      meta:{
        noAuthorization:true
      }
    },
    {
      path:'/index',
      name:'index',
      redirect:"/index/users",
      component:() => import('../views/Index.vue'),
      children:[
        {
          path:"users",
          name:"users",
          component:() => import('../views/Users.vue'),
        },
        {
          path:"roles",
          name:"roles",
          component:() => import('../views/rights/Roles.vue'),
        },
        {
          path:"rights",
          name:"rights",
          component:() => import('../views/rights/Rights.vue'),
        },
        {
          path:"goods",
          name:"goods",
          component:() => import('../views/goods/Goods.vue'),
        },
        {
          path:"params",
          name:"params",
          component:() => import('../views/goods/Params.vue'),
        },
        {
          path:"categories",
          name:"categories",
          component:() => import('../views/goods/Categories.vue'),
        },
        {
          path:"orders",
          name:"orders",
          component:() => import('../views/Orders.vue'),
        },
        {
          path:"reports",
          name:"reports",
          component:() => import('../views/Reports.vue'),
        },

      ]
    },
    {
      path:"*",
      redirect:"/"
    }
  // {
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]
const router = new VueRouter({
  routes
})
router.beforeEach((to,from,next)=>{
  if(!to.meta.noAuthorization){
    if(store.state.token){
      next()
    }else{
      next({
        path:"/login"
      })
    }
  }else{
    next()
  }
})

export default router
