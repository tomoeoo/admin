import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'reset-css'
import MyPlugin from './plugins'
import dayjs from 'dayjs'
import echarts from 'echarts'
import ECharts from 'vue-echarts'
Vue.component('v-chart', ECharts)


Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(MyPlugin)
Vue.prototype.$echarts = echarts
Vue.filter("dealDate",(val)=>{
  return dayjs(val).format('YYYY-MM-DD HH:mm:ss')
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
